Simple photo conversion script for 1024x600 tablet
==================================================


We needed a script to convert photos for Mum's 1024x600 tablet, that
we use in landscape-mode as a digital frame. The photo display application
resizes according to width (1024px), however portrait photos would be slowly
scrolled over instead of shown height-resized.

Furthermore, we preferred to use smallest files possible, to save space
on Mum's device

Usage:
------

The script would resize all *landscape* photos to 1024x768 px scale.
(Note: I rather chose 1024x768 format, than the odd 1024x600 size -- the 
difference will be handled by the device correctly anyway.)

Regarding *portrait* photos, two options are available:
- placing them on a 768x1024px black background (will be shown at once)
- re-sizing to 1024x1365px (will be scrolled)

```
Usage: mum_photo_convert.sh [-p <postfix>] <input_dir> [<output_dir>]

Processing contents of <input_dir>, copying files to <output_dir> (default: ./)
    -p <postfix>    Postfix of the new files (default: '_small')
    -r              Recursive search for .jpg files
    -s              Resize portrait images so that they would be scrolled on tablet
```


Dependencies:
-------------
- ImageMagick
- coreutils
