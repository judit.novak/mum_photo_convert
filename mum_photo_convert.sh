#!/bin/bash

# Author: Judit Novak <judit.novak@gmail.com>
# 
# We needed a script to convert photos for Mum's 1024x600 tablet, that
# we use in landscape-mode as a digital frame. The photo display application
# resizes according to width (1024px), however portrait photos would be slowly
# scrolled over instead of shown height-resized.
#
# Furthermore, we preferred to use smallest files possible, to save space
# on Mum's device
#
# The script would resize all landscape photos to 1024x768 px scale.
# Regarding portrait photos, two options are available:
# 	- placing them on a 768x1024px black background (will be shown at once)
#	- re-sizing to 1024x1365px (will be scrolled)
#
# Dependencies: 
# 	* ImageMagick
#	* coreutils


POSTFIX='_small'
FORMAT='.jpg'
OUTDIR='mums_photos'
SCROLL_PORTRAIT=0

FIND_PARAMS='-maxdepth 1'

help () {

	SCRIPT_NAME=`basename $0`
	echo "Usage: $SCRIPT_NAME [-p <postfix>] <input_dir> [<output_dir>]"
	echo
	echo "Processing contents of <input_dir>, copying files to <output_dir> (default: ./)"
	echo "    -p <postfix>    Postfix of the new files (default: '_small')" 
	echo "    -r              Recursive search for $FORMAT files"
	echo "    -s              Resize portrait images so that they would be scrolled on tablet"

	exit
}

if (( $# == 0 ))
then
	help
fi

while (( $# != 0 ))
do
	case $1 in 
		-h|--help)
			help
			exit
			;;
		-p)	
			POSTFIX=$2
			shift
			shift
			;;
		-r)
			FIND_PARAMS=''
			shift
			;;
		-s)
			SCROLL_PORTRAIT=1
			shift
			;;
		*)
			if [[ $INDIR == '' ]]
			then
				INDIR=$1
			else
				OUTDIR=$1
			fi
			shift
			;;
	esac
done

OUTDIR="${OUTDIR%%//*}/"
for INFILE in `find $INDIR -name '*'"$FORMAT" ${FIND_PARAMS} -not -path "*$OUTDIR/*" 2>/dev/null`
do

	# Creating output directory (structure) if needed
	BASEDIR=`dirname $INFILE`
	INFILE_NAME=`basename $INFILE`
	INFILE=${INFILE#./}
	OUTBASE_DIR=""
	if [[ $BASEDIR == '.' ]]
	then
		# Creating new output directory (if needed)
		mkdir -p ${OUTDIR}
	else
		# Creating subdirectories, if input was recursive
		FULL_INDIR=`realpath $INDIR`
		OUTBASE_DIR=`echo $BASEDIR | sed -e "s%${FULL_INDIR}%%"`
		OUTBASE_DIR="${OUTBASE_DIR#/}"					# Cosmetics: potential trailing '/'
		mkdir -p ${OUTDIR}/${OUTBASE_DIR}
	fi

    if [[ $OUTBASE_DIR ]]
    then
        OUTBASE_DIR="${OUTBASE_DIR}/"
    fi

	OUTFILE="${OUTDIR}${OUTBASE_DIR}"${INFILE_NAME/${FORMAT}/${POSTFIX}${FORMAT}}

    SHORT_WIDTH=`identify -format '%w %h' $INFILE | awk ' /[0-9]{1,4} / { print ($1 < $2) }'`
    EXIM_PORTRAIT_INFO=`identify -format '%[orientation] %[EXIF:Orientation]' $INFILE \
                        | awk '/[a-zA-Z-]* / { print ($1 ~ /((R|r)ight-?(T|t)op)|((L|l)eft-?(B|b)ottom)/) || ($2==6) || ($2==8) }'`

 	ORIENT='landscape'
 	CONV_PARAMS='-geometry 1024x'
    if [[ $SHORT_WIDTH == "" ]] || [[ $EXIM_PORTRAIT_INFO == "" ]]
    then
        echo "[ERROR] When processing ${INFILE}, can't determine geometry/orientation"
        continue
    fi

    if (( $SHORT_WIDTH || $EXIM_PORTRAIT_INFO ))
 	then
 		ORIENT='portrait'
 		if (( ! $SCROLL_PORTRAIT ))
 		then
 			CONV_PARAMS='-resize x768 -background black -gravity center -extent 1024x768'
 		fi
 	fi
    echo "Converting ${INFILE} (${ORIENT}) into ${OUTFILE} (using params: ${CONV_PARAMS})"
 	convert -auto-orient ${CONV_PARAMS} $INFILE $OUTFILE

done

